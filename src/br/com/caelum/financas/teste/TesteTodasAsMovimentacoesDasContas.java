package br.com.caelum.financas.teste;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.caelum.financas.modelo.Conta;
import br.com.caelum.financas.modelo.Movimentacao;
import br.com.caelum.financas.util.JPAUtil;

public class TesteTodasAsMovimentacoesDasContas {

	public static void main(String[] args) {
		
		EntityManager em = new JPAUtil().getEntityManager();
		em.getTransaction().begin();
		
		//Na query comentada abaixo, � retornado apenas as contas. As movimentacoes sao retornadas apenas no foreach de contas.
		//Isso acontece pois "List" s�o chamados de LazyLoading, ou seja, os resultados s� serao retornados quando forem necessario.
		//Dentro do foreach de conta, o JPA far� um select para buscar movimentacoes para cada conta, gerando N + 1 querys.
		//String jpql = "select c from Conta c";
		
		//Para resolver a questao do LazyLoading � utilizado um join, que forca o retorno de contas e movimentacoes em apenas uma query.
		String jpql = "select distinct c from Conta c left join fetch c.movimentacoes";
		
		Query query = em.createQuery(jpql);
		
		List<Conta> todasAsContas = query.getResultList();
		
		for (Conta conta : todasAsContas) {
			System.out.println("Titular: " + conta.getTitular());
			System.out.println("Movimenta��es: ");
			System.out.println(conta.getMovimentacoes());
		}
		
		em.getTransaction().commit();
		em.close();			
	}
}
