package br.com.caelum.financas.teste;

import javax.persistence.EntityManager;

import br.com.caelum.financas.modelo.Conta;
import br.com.caelum.financas.util.JPAUtil;

public class TesteBuscaConta {
	
	public static void main(String[] args) {
		
		EntityManager em = new JPAUtil().getEntityManager();
		em.getTransaction().begin();
		
		//Busca uma conta, passando qual � a entidade e o seu id
		Conta conta = em.find(Conta.class, 1);
		
		System.out.println(conta.getTitular());
				
		//O metodo find retorna uma instancia do objeto no estado "Managed", onde os dados da entidade sao sincronizados automaticamente com o banco de dados.
		//Por este motivo, ao utilizar um set apos o find, o valor do banco sera alterado.
		conta.setTitular("Wall");
		conta.setAgencia("000");

		em.getTransaction().commit();
	}
}
