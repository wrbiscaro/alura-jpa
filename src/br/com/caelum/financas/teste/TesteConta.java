package br.com.caelum.financas.teste;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.caelum.financas.modelo.Conta;
import br.com.caelum.financas.util.JPAUtil;

public class TesteConta {

	public static void main(String[] args) {
		Conta conta = new Conta();
		conta.setTitular("Wallace");
		conta.setBanco("Caixa Econ�mica Federal");
		conta.setAgencia("111");
		conta.setNumero("789");
		
		EntityManager em = new JPAUtil().getEntityManager();
		
		//Antes de fazer uma persistencia � necessario abrir uma transacao. O estado de uma entidade antes de fazer a persistencia � chamado de "Transient", ou seja, nunca foi para o banco e se a aplicacao morrer ela morre junto.
		em.getTransaction().begin();
		//Persiste a conta
		em.persist(conta);
		//Apos a persistencia, o estado da entidade muda para "Managed", ou seja, sincronizada com o banco.
		//Quando a transacao nao estiver mais em uso, � necessario commita-la
		em.getTransaction().commit();
		
		//Boa pratica fechar o EntityManager apos a transacao.		
		em.close();
		
		//Apos fechar o EntityManager, o estado da entidade muda para "Detached", ou seja, ja foi gerenciado pela JPA um dia e n�o � mais.
		//Caso seja necessario, � poss�vel continuar usando a mesma entidade sem fazer um "find", bastando abrir um novo EntityManager e usar o metodo "merge" (codigo comentado abaixo). 
		/*
		EntityManager em2 = new JPAUtil().getEntityManager();
		em2.getTransaction().begin();

		conta.setTitular("Wall");
		em2.merge(conta);

		em2.getTransaction().commit();
		em2.close();
		*/
		
		//Busca uma entidade, passando ela pro estado managed para poder ser excluida com o metodo remove
		EntityManager em2 = new JPAUtil().getEntityManager();
		em2.getTransaction().begin();

		conta = em2.find(Conta.class, 5);
		em2.remove(conta);

		em2.getTransaction().commit();
		em2.close();
	}
}
