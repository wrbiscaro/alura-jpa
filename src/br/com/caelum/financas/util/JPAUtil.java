package br.com.caelum.financas.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {

	//EntityManagerFactory � a classe que sabe criar o EntityManager. Persistence representa o persistence.xml e "financas" representa uma unidade de persistencia que esta nesse arquivo.
	private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("financas");
	
	//EntityManager � a classe principal para usar a JPA
	public EntityManager getEntityManager() {
		return emf.createEntityManager();
	}
}
