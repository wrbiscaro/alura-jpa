package br.com.caelum.financas.modelo;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity //Diz ao Hibernate que a classe � uma entidade e sera persistida, assim ele cria a tabela correspondente, faz as configuracoes, etc.
public class Conta {

	@Id //Anotacao para o Hibernate identificar a chave primaria
	@GeneratedValue(strategy=GenerationType.IDENTITY) //Pede para o Hibernate gerenciar o valor dessa chave, seja por auto incremento, sequence ou outros (IDENTITY � auto incremento).
	private Integer id;
	
	private String titular;
	private String numero;
	private String banco;
	private String agencia;
	
	//Uma conta pode ter varias movimentacoes.
	//Relacionamento bidirecional. Mappedby indica que o relacionamento ja foi criado no atributo "conta" da parte "many" do relacionamento e ele � a parte forte do relacionamento. A parte forte do relacionamento � geralmente a chave estrangeira da tabela de "muitos".
	@OneToMany(mappedBy="conta")  
	private List<Movimentacao> movimentacoes;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitular() {
		return titular;
	}
	public void setTitular(String titular) {
		this.titular = titular;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getBanco() {
		return banco;
	}
	public void setBanco(String banco) {
		this.banco = banco;
	}
	public String getAgencia() {
		return agencia;
	}
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public List<Movimentacao> getMovimentacoes() {
		return movimentacoes;
	}
}
